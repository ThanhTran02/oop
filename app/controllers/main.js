import { ListPerson, Employee, Customer, Student } from "../models/model.js";
import { filterPerson, getInfo, showInfo, sortByFullName, setInfo, validate } from "./controller.js";

const person1 = new Student("1234", "John Doe", "123 Main St", "john@example.com", 90, 85, 95);
const person2 = new Employee("5678", "Jane Smith", "456 Elm St", "jane@example.com", 20, 50);
const person3 = new Customer("9012", "Acme Inc.", "789 Oak St", "info@acme.com", "Acme Inc.", 1000, 4.5);

const list = new ListPerson();
list.addPerson(person1);
list.addPerson(person2);
list.addPerson(person3);

let listArr = list.getAllPersons();
showInfo(listArr);

function sortName() {
    listArr = sortByFullName(listArr);
    showInfo(listArr);
};
window.sortName = sortName;
function filter() {
    const typePerson = document.getElementById("filterType").value * 1;
    let listPer = filterPerson(listArr, typePerson);
    showInfo(listPer);
    if (!!typePerson == false)
        showInfo(listArr);
}
window.filter = filter;
function add() {
    const typePerson = document.getElementById("typePerson").value * 1;
    list.addPerson(getInfo(typePerson));
    listArr = list.getAllPersons();
    showInfo(listArr);

}
window.add = add;
function deletePerson(idPerson) {
    list.removePerson(idPerson);
    listArr = list.getAllPersons();
    showInfo(listArr);

}
window.deletePerson = deletePerson;

function fix(idPerson) {
    setInfo(list.getPersonByID(idPerson));
}
window.fix = fix;
function fixPerson() {
    const id = document.getElementById("idPerson").value;
    let typePerson = list.getTypes(id);
    console.log(typePerson);
    if (typePerson == 'Student')
        typePerson = 1;
    else
        if (typePerson == 'Employee')
            typePerson = 2;
        else
            typePerson = 3;
    // list.removePerson(idPerson);
    // console.log(getInfo(typePerson));
    // list.addPerson(getInfo(typePerson));
    // console.log("🚀 ~ file: main.js:54 ~ fixPerson ~ typePerson:", typePerson)
    list.updatePerson(id, getInfo(typePerson))
    console.log(list);
    let listArr = list.getAllPersons();
    showInfo(listArr);
}
window.fixPerson = fixPerson;
function calScore(idPerson) {
    const person = list.getPersonByID(idPerson);
    console.log(person.calculateScore());
    document.getElementById("score").innerHTML = `<h3>    
    Điểm trung bình của học viên là : ${person.calculateScore()}</h3>`
}

window.calScore = calScore;

function calSalary(idPerson) {
    const person = list.getPersonByID(idPerson);
    document.getElementById("score").innerHTML = `<h3>    
    Lương của nhân viên : ${person.calculateSalary()} $</h3>`
}

window.calSalary = calSalary;
function checkdata() {
    const typePerson = document.getElementById("typePerson").value * 1;
    console.log(getInfo(typePerson));
    validate(getInfo(typePerson));
}
window.checkdata = checkdata;