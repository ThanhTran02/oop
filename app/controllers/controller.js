import { ListPerson, Employee, Customer, Student } from "../models/model.js";
import { checkName, checkNone } from "../validate/validate.js";
export function showInfo(list) {
    let contentHTML = "";
    list.forEach((person) => {
        let content = `<tr>
        <td>${person.id}</td>
        <td>${person.name}</td>
        <td>${person.addr}</td>
        <td>${person.email}</td>
        `
        if (person instanceof Student) {
            content += `
            <td>${person.math}</td>
            <td>${person.physical}</td>
            <td>${person.chemical}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> 
                <button onclick = "calScore(${person.id})" data-modal-target="popup-modal" data-modal-toggle="popup-modal"
                class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r"
                type="button">
                Tính điểm trung bình
                </button>
            </td> `

        } else if (person instanceof Employee) {
            content += `
            <td></td>
            <td></td>
            <td></td>
            <td>${person.day}</td>
            <td>${person.salary}</td>
            <td></td>
            <td></td>
            <td></td>
            <td> 
                <button onclick = "calSalary(${person.id})" data-modal-target="popup-modal" data-modal-toggle="popup-modal"
                class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r"
                type="button">
                Tính Lương
                </button>
            </td>`
        } else if (person instanceof Customer) {
            content += `
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>${person.company}</td>
            <td>${person.bill}</td>
            <td>${person.evalu}</td>`
        };
        content += `<td>
            <div class="inline-flex">
                <button  onclick="fix(${person.id})" data-modal-target="defaultModal" data-modal-toggle="defaultModal" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l flex-inline">
                    Sửa<i class="fa fa-edit"></i>
                </button>
                <button onclick ="deletePerson(${person.id})" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                    Xoá <i class="fa fa-trash-alt"></i>
                </button>
            </div>
            </td>
        </tr>`
        contentHTML += content;
    });
    document.getElementById("data").innerHTML = contentHTML;
}
export function sortByFullName(list) {
    list.sort(function (a, b) {
        var nameA = a.name.toUpperCase();
        var nameB = b.name.toUpperCase();

        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        return 0;
    });

    return list;
}
export function filterPerson(list, typePerson) {
    let arrPerson = [];

    list.forEach((person) => {
        console.log(typePerson);
        switch (typePerson) {
            case 1:
                if (person instanceof Student) {
                    arrPerson.push(person);
                };
                break;
            case 2:
                if (person instanceof Employee) {
                    arrPerson.push(person);
                };
                break;
            case 3:
                if (person instanceof Customer) {
                    arrPerson.push(person);
                }
                break;
        }
    })
    return arrPerson;
}

export function getInfo(typePerson) {
    let id = document.getElementById("idPerson").value;
    let name = document.getElementById("name").value;
    let addr = document.getElementById("addr").value;
    let email = document.getElementById("email").value;
    // Student
    let math = document.getElementById("math").value;
    let physical = document.getElementById("physical").value;
    let chemical = document.getElementById("chemical").value;
    // Customer
    let company = document.getElementById("company").value;
    let bill = document.getElementById("bill").value;
    let evaluation = document.getElementById("evaluation").value;
    // Employee
    let day = document.getElementById("day").value;
    let salary = document.getElementById("salary").value;
    switch (typePerson) {
        case 1:
            return new Student(
                id,
                name,
                addr,
                email,
                math,
                physical,
                chemical,
                // company,
                // bill,
                // evaluation,
                // day,
                // salary,
            )
        case 2:
            return new Employee(
                id,
                name,
                addr,
                email,
                // math,
                // physical,
                // chemical,
                // company,
                // bill,
                // evaluation,
                day,
                salary,
            )
        case 3:
            return new Customer(
                id,
                name,
                addr,
                email,
                // math,
                // physical,
                // chemical,
                company,
                bill,
                evaluation,
                // day,
                // salary,
            )

    }


}
export function setInfo(person) {
    document.getElementById("idPerson").value = person.id;
    document.getElementById("name").value = person.name;
    document.getElementById("addr").value = person.addr;
    document.getElementById("email").value = person.email;
    document.getElementById("math").value = person.math;
    document.getElementById("physical").value = person.physical;
    document.getElementById("chemical").value = person.chemical;
    document.getElementById("company").value = person.company;
    document.getElementById("bill").value = person.bill;
    document.getElementById("evaluation").value = person.evaluation;
    document.getElementById("day").value = person.day;
    document.getElementById("salary").value = person.salary;

}
export function validate(person) {
    var isValid = checkNone("tbName", person.name) && checkName("tbName", person.name);
    isValid &= checkNone("tbID", person.id);
    isValid &= checkNone("tbAddr", person.addr);
    isValid &= checkNone("tbEmail", person.email);

    return isValid;
}
