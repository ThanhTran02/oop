export class Person {
    constructor(id, name, addr, email) {
        this.id = id;
        this.name = name;
        this.addr = addr;
        this.email = email;
    }
}

export class Student extends Person {
    constructor(id, name, addr, email, math, physical, chemical) {
        super(id, name, addr, email);
        this.math = math;
        this.physical = physical;
        this.chemical = chemical;
    }
    calculateScore() {
        return (this.math + this.physical + this.chemical) / 3;
    }
}
export class Employee extends Person {
    constructor(id, name, addr, email, day, salary) {
        super(id, name, addr, email);
        this.day = day;
        this.salary = salary;
    }
    calculateSalary() {
        return this.day * this.salary;
    }
}
export class Customer extends Person {
    constructor(id, name, addr, email, company, bill, evalu) {
        super(id, name, addr, email);
        this.company = company;
        this.bill = bill;
        this.evalu = evalu;
    }
}
export class ListPerson {
    constructor() {
        this.persons = [];
    }
    addPerson(person) {
        this.persons.push(person);
    }
    removePerson(id) {
        const index = this.persons.forEach((person) => {
            person.id == id;
        });
        if (index != -1) {
            this.persons.splice(index, 1)
        }
    }
    getPersonByID(id) {
        const person = this.persons.find((person) => person.id == id);
        return person;

    }
    getTypes(idPerson) {
        const person = this.persons.find((person) => person.id == idPerson);
        console.log(person);
        if (person) {
            return person.constructor.name;
        }
    }
    updatePerson(personId, newData) {
        const person = this.getPersonByID(personId);
        if (person) {
            Object.assign(person, newData);
        }

    }
    getAllPersons() {
        return this.persons;
    }
    getObjectTypes() {
        return this.persons.map((person) => person.constructor.name);
    }
    showAllPersonsInfo() {
        let contentHTML = "";
        this.persons.forEach((person) => {
            let content = `<tr>
            <td>${person.id}</td>
            <td>${person.name}</td>
            <td>${person.addr}</td>
            <td>${person.email}</td>
            `
            if (person instanceof Student) {
                content += `
                <td>${person.math}</td>
                <td>${person.physical}</td>
                <td>${person.chemical}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                `

            } else if (person instanceof Employee) {
                content += `
                <td></td>
                <td></td>
                <td></td>
                <td>${person.day}</td>
                <td>${person.salary}</td>
                <td></td>
                <td></td>
                <td></td>`
            } else if (person instanceof Customer) {
                content += `
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>${person.company}</td>
                <td>${person.bill}</td>
                <td>${person.evalu}</td>`
            };
            content += `<td>
                <div class="inline-flex">
                    <button  onclick="fix(${person.id})" data-modal-target="defaultModal" data-modal-toggle="defaultModal" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l flex-inline">
                        Sửa<i class="fa fa-edit"></i>
                    </button>
                    <button onclick ="deleteProduct(${person.id})" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                        Xoá <i class="fa fa-trash-alt"></i>
                    </button>
                </div>
                </td>
            </tr>`
            contentHTML += content;
        });
        document.getElementById("data").innerHTML = contentHTML;
    }
}