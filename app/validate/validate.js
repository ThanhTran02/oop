function showMessage(idSpan, message) {
    document.getElementById(idSpan).innerText = message;
}

export function checkPrice(idSpan, price) {

    if (price < 0) {
        showMessage(idSpan, "Giá Trị không hợp lệ")
        return false;
    }
    else {
        showMessage(idSpan, "")
        return true;
    }
}
export function checkNone(idSpan, value) {
    if (value == '') {
        showMessage(idSpan, 'Vui lòng điền giá trị vào');
        return false;
    }
    else {
        showMessage(idSpan, '');
        return true;
    }
}
export function checkName(idSpan, value) {
    var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;
    if (!regName.test(value)) {
        showMessage(idSpan, 'Tên không phù hợp');
        return false;
    } else {
        showMessage(idSpan, '');
        return true;

    }
}
